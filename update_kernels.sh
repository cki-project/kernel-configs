#!/bin/bash
set -eux -o pipefail

ARCHITECTURES=(aarch64 ppc64le s390x x86_64)
KERNELS_DIR=/tmp/kernels

mkdir -vp $KERNELS_DIR

pushd $KERNELS_DIR
for ARCH in ${ARCHITECTURES[@]}; do
    dnf download -y --enablerepo=updates-testing \
        --forcearch $ARCH --destdir . kernel-core
    VERSIONSTRING=$(rpm --queryformat="%{VERSION}.%{RELEASE}" -qp *.rpm)
    rpm2archive kernel-core-*.${ARCH}.rpm
    tar --extract --to-stdout --wildcards --gunzip -f *.tgz \
        "./lib/modules/*/config" > ${CI_PROJECT_DIR}/${ARCH}.config
    rm -rfv *.rpm *.tgz
done
popd

git config user.name "CKI@GitLab"
git config user.email "cki-project@redhat.com"
git add *.config
git status

# Did anything change?
LINES_CHANGED=$(git diff --cached | wc -l)
if [ "${LINES_CHANGED}" != "0" ]; then
    git commit -m "Updated: $VERSIONSTRING"
    git remote add gitlab git@gitlab.com:cki-project/kernel-configs.git
    ssh-keyscan -H gitlab.com >> ~/.ssh/known_hosts
    git push gitlab HEAD:master
fi
