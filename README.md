kernel-configs
==============

A collection of kernel configurations used to build kernels for testing in CKI
project. All configs are automatically updated with GitLab CI scheduled jobs.
